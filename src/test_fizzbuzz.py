import unittest
from fizzbuzz import fizzbuzz

class TestFizzBuzz(unittest.TestCase):
    def test_fizzbuzz(self):
        expected = [
            '1', '2', 'Fizz', '4', 'Buzz', 'Fizz',
            '7', '8', 'Fizz', 'Buzz', '11', 'Fizz',
            '13', '14', 'FizzBuzz'
        ]
        actual = fizzbuzz(15)
        self.assertListEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()

