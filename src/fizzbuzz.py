# def fizzbuzz(num=10, start=1):
#     for i in range(start, num + 1):
#         if i % 15 == 0:
#             print('FizzBuzz')
#         elif i % 5 == 0:
#             print('Buzz')
#         elif i % 3 == 0:
#             print('Fizz')
#         else:
#             print(str(i))

# def fizzbuzz(num=10, start=1):
#     result = []
#     for i in range(start, num + 1):
#         if i % 15 == 0:
#             result.append('FizzBuzz')
#         elif i % 5 == 0:
#             result.append('Buzz')
#         elif i % 3 == 0:
#             result.append('Fizz')
#         else:
#             result.append(str(i))
#     return result

def fizzbuzz(num=10, start=1):
    result = []
    for i in range(start, num + 1):
        fb_str = ''
        if i % 3 == 0:
            fb_str += 'Fizz'
        if i % 5 == 0:
            fb_str += 'Buzz'
        if len(fb_str) == 0:
            fb_str += str(i)
        result.append(fb_str)

    return result

if __name__ == '__main__':
    import os
    print(os.linesep.join(fizzbuzz(15)))
