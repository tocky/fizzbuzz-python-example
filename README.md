# Python での FizzBuzz 実装例

[第8回 ZEALOT 勉強会](https://docs.google.com/presentation/d/1Er-hpOrvedKpBl-qMw3UmpSF1LSpVMZVbkdMeSXCusg/edit?usp=sharing)の中で、単体テストについて説明した際に利用した簡易な Python コードを含んだリポジトリです。

単純な FizzBuzz の実装を見せ、そのコードを unittest からテストしやすくする過程を示しました。
